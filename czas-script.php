<?php
	function minutaNaSekunda($wartosc){

		return $wartosc * 60;
	}

	function godzinaNaSekunda($wartosc){

		return $wartosc * 3600;
	}

	function dobaNaSekunda($wartosc){

		return $wartosc * 86400;
	}

	function tydzienNaSekunda($wartosc){

		return $wartosc * 604800;
	}

	function miesiacNaSekunda($wartosc){

		return $wartosc * 2628000;
	}

	function rokNaSekunda($wartosc){

		return $wartosc * 31536000;
	}

	function sekundaNaMinuta($wartosc){

		return $wartosc * 0.0166666667;
	}

	function sekundaNaGodzina($wartosc){

		return $wartosc * 0.0002777778;
	}

	function sekundaNaDoba($wartosc){

		return $wartosc * 0.0000115741;
	}

	function sekundaNaTydzien($wartosc){

		return $wartosc * 0.0000016534;
	}

	function sekundaNaMiesiac($wartosc){

		return $wartosc * 0.0000003805;
	}

	function sekundaNaRok($wartosc){

		return $wartosc * 0.0000000317;
	}

	include("calc.html");

	if($_POST['zmienna'] != NULL && is_numeric($_POST['zmienna'])){

		echo $_POST['zmienna'] . " (" . $_POST['poczatkowa'] . ") = ";

		if($_POST['poczatkowa'] == "sekunda"){

			if($_POST['koncowa'] == "sekunda"){

				echo $_POST['zmienna'] . "(sekunda)";
			}
			else if($_POST['koncowa'] == "minuta"){

				echo sekundaNaMinuta($_POST['zmienna']) . "(minuta)";
			}
			else if($_POST['koncowa'] == "godzina"){

				echo sekundaNaGodzina($_POST['zmienna']) . "(godzina)";
			}
			else if($_POST['koncowa'] == "doba"){

				echo sekundaNaDoba($_POST['zmienna']) . "(doba)";
			}
			else if($_POST['koncowa'] == "tydzien"){

				echo sekundaNaTydzien($_POST['zmienna']) . "(tydzien)";
			}
			else if($_POST['koncowa'] == "miesiac"){

				echo sekundaNaMiesiac($_POST['zmienna']) . "(miesiac)";
			}
			else if($_POST['koncowa'] == "rok"){

				echo sekundaNaRok($_POST['zmienna']) . "(rok)";
			}
		}
		else if($_POST['poczatkowa'] == "minuta"){

			$pomocnicza = minutaNaSekunda($_POST['zmienna']);

			if($_POST['koncowa'] == "sekunda"){

				echo $pomocnicza . "(sekunda)";
			}
			else if($_POST['koncowa'] == "minuta"){

				echo $_POST['zmienna'] . "(minuta)";
			}
			else if($_POST['koncowa'] == "godzina"){

				echo sekundaNaGodzina($pomocnicza) . "(godzina)";
			}
			else if($_POST['koncowa'] == "doba"){

				echo sekundaNaDoba($pomocnicza) . "(doba)";
			}
			else if($_POST['koncowa'] == "tydzien"){

				echo sekundaNaTydzien($pomocnicza) . "(tydzien)";
			}
			else if($_POST['koncowa'] == "miesiac"){

				echo sekundaNaMiesiac($pomocnicza) . "(miesiac)";
			}
			else if($_POST['koncowa'] == "rok"){

				echo sekundaNaRok($pomocnicza) . "(rok)";
			}
		}
		else if($_POST['poczatkowa'] == "godzina"){

			$pomocnicza = godzinaNaSekunda($_POST['zmienna']);

			if($_POST['koncowa'] == "sekunda"){

				echo $pomocnicza . "(sekunda)";
			}
			else if($_POST['koncowa'] == "minuta"){

				echo sekundaNaMinuta($pomocnicza). "(minuta)";
			}
			else if($_POST['koncowa'] == "godzina"){

				echo $_POST['zmienna'] . "(godzina)";
			}
			else if($_POST['koncowa'] == "doba"){

				echo sekundaNaDoba($_POST['zmienna']) . "(doba)";
			}
			else if($_POST['koncowa'] == "tydzien"){

				echo sekundaNaTydzien($pomocnicza) . "(tydzien)";
			}
			else if($_POST['koncowa'] == "miesiac"){

				echo sekundaNaMiesiac($pomocnicza) . "(miesiac)";
			}
			else if($_POST['koncowa'] == "rok"){

				echo sekundaNaRok($pomocnicza) . "(rok)";
			}
		}
		else if($_POST['poczatkowa'] == "doba"){

			$pomocnicza = dobaNaSekunda($_POST['zmienna']);

			if($_POST['koncowa'] == "sekunda"){

				echo $pomocnicza . "(sekunda)";
			}
			else if($_POST['koncowa'] == "minuta"){

				echo sekundaNaMinuta($pomocnicza). "(minuta)";
			}
			else if($_POST['koncowa'] == "godzina"){

				echo sekundaNaGodzina($pomocnicza) . "(godzina)";
			}
			else if($_POST['koncowa'] == "doba"){

				echo $_POST['zmienna'] . "(doba)";
			}
			else if($_POST['koncowa'] == "tydzien"){

				echo sekundaNaTydzien($pomocnicza) . "(tydzien)";
			}
			else if($_POST['koncowa'] == "miesiac"){

				echo sekundaNaMiesiac($pomocnicza) . "(miesiac)";
			}
			else if($_POST['koncowa'] == "rok"){

				echo sekundaNaRok($pomocnicza) . "(rok)";
			}
		}
		else if($_POST['poczatkowa'] == "tydzien"){

			$pomocnicza = tydzienNaSekunda($_POST['zmienna']);

			if($_POST['koncowa'] == "sekunda"){

				echo $pomocnicza . "(sekunda)";
			}
			else if($_POST['koncowa'] == "minuta"){

				echo sekundaNaMinuta($pomocnicza). "(minuta)";
			}
			else if($_POST['koncowa'] == "godzina"){

				echo sekundaNaGodzina($pomocnicza) . "(godzina)";
			}
			else if($_POST['koncowa'] == "doba"){

				echo sekundaNaDoba($_POST['zmienna']) . "(doba)";
			}
			else if($_POST['koncowa'] == "tydzien"){

				echo $_POST['zmienna'] . "(tydzien)";
			}
			else if($_POST['koncowa'] == "miesiac"){

				echo sekundaNaMiesiac($pomocnicza) . "(miesiac)";
			}
			else if($_POST['koncowa'] == "rok"){

				echo sekundaNaRok($pomocnicza) . "(rok)";
			}
		}
		else if($_POST['poczatkowa'] == "miesiac"){

			$pomocnicza = miesiacNaSekunda($_POST['zmienna']);

			if($_POST['koncowa'] == "sekunda"){

				echo $pomocnicza . "(sekunda)";
			}
			else if($_POST['koncowa'] == "minuta"){

				echo sekundaNaMinuta($pomocnicza). "(minuta)";
			}
			else if($_POST['koncowa'] == "godzina"){

				echo sekundaNaGodzina($pomocnicza) . "(godzina)";
			}
			else if($_POST['koncowa'] == "doba"){

				echo sekundaNaDoba($_POST['zmienna']) . "(doba)";
			}
			else if($_POST['koncowa'] == "tydzien"){

				echo sekundaNaTydzien($pomocnicza) . "(tydzien)";
			}
			else if($_POST['koncowa'] == "miesiac"){

				echo $_POST['zmienna'] . "(miesiac)";
			}
			else if($_POST['koncowa'] == "rok"){

				echo sekundaNaRok($pomocnicza) . "(rok)";
			}
		}
		else if($_POST['poczatkowa'] == "rok"){

			$pomocnicza = rokNaSekunda($_POST['zmienna']);

			if($_POST['koncowa'] == "sekunda"){

				echo $pomocnicza . "(sekunda)";
			}
			else if($_POST['koncowa'] == "minuta"){

				echo sekundaNaMinuta($pomocnicza). "(minuta)";
			}
			else if($_POST['koncowa'] == "godzina"){

				echo sekundaNaGodzina($pomocnicza) . "(godzina)";
			}
			else if($_POST['koncowa'] == "doba"){

				echo sekundaNaDoba($_POST['zmienna']) . "(doba)";
			}
			else if($_POST['koncowa'] == "tydzien"){

				echo sekundaNaTydzien($pomocnicza) . "(tydzien)";
			}
			else if($_POST['koncowa'] == "miesiac"){

				echo sekundaNaMiesiac($pomocnicza) . "(miesiac)";
			}
			else if($_POST['koncowa'] == "rok"){

				echo $_POST['zmienna'] . "(rok)";
			}
		}
	}
?>