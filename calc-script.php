<html>
	<head>
		<title>Konwerter</title>
		<meta http-equiv="Content-Type"
			content="text/html; charset=iso-8859-2" />
	</head>
	<body >
			<?php
				include("calc.html");
				if($_POST['zmienna'] != NULL && is_numeric($_POST['zmienna'])){

					echo $_POST['zmienna'] . " (" . $_POST['poczatkowa'] . ") = ";

					switch($_POST['poczatkowa']){

						case "kelwin":

							switch($_POST['koncowa']){

								case "kelwin":

									echo $_POST['zmienna'] . " (kelwin)";
									break;

								case "celcius":

									$pomocnicza = $_POST['zmienna'] - 273.15;
									echo $pomocnicza . " (celcius)";
									break;

								case "fahrenheit":

									$pomocnicza = ((9/5)*($_POST['zmienna'] - 273)) + 32;
									echo $pomocnicza . " (fahrenheit)";
									break;
							}

							break;

						case "celcius":

							switch($_POST['koncowa']){

								case "kelwin":

									$pomocnicza = $_POST['zmienna'] + 273.15;
									echo $pomocnicza . " (kelwin)";
									break;

								case "celcius":

									echo $_POST['zmienna'] . " (celcius)";
									break;

								case "fahrenheit":

									$pomocnicza = 32 + ((9/5)* $_POST['zmienna']);
									echo $pomocnicza . " (fahrenheit)";
									break;
							}

							break;

						case "fahrenheit":

							switch($_POST['koncowa']){

								case "kelwin":

									$pomocnicza = (($_POST['zmienna'] - 32) * (5/9)) + 273;
									echo $pomocnicza . " (kelwin)";
									break;

								case "celcius":

									$pomocnicza = (5/9)*($_POST['zmienna'] - 32);
									echo $pomocnicza . " (celcius)";
									break;

								case "fahrenheit":

									echo $_POST['zmienna'] . " (fahrenheit)";
									break;
							}

							break;
					}
				}
				else{

					printf("Warto�ci nie s� liczbami albo nie poda�e� �adnej warto�ci.");
				}
			?>
		</font>
	</body>
</html>
