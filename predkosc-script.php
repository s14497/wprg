<?php

	function msNaKmh ($wartosc){
		return $wartosc * 3.6;
	}

	function kmhNaMs ($wartosc){
		return $wartosc * 0.2777778;
	}
	
	function msNaMph($wartosc){
		return $wartosc * 2.23694;
	}
	
	function mphNaMs($wartosc){
		return $wartosc * 0.44704;
	}
	
	function msNaKt($wartosc){
		return $wartosc * 1.94384;
	}

	function ktNaMs($wartosc){
		return $wartosc * 0.51444;
	}
	
	function msNaMa ($wartosc){
		return $wartosc * 0.00294;
	}

	function maNaMs ($wartosc){
		return $wartosc * 340;
	}
	
	function msNaC ($wartosc){
		return $wartosc * 0.0000000033;
	}

	function cNaMs ($wartosc){
		return $wartosc * 299792462.67;
	}
	
	include("calc.html");

	if($_POST['zmienna'] != NULL && is_numeric($_POST['zmienna'])){

		echo $_POST['zmienna'] . " (" . $_POST['poczatkowa'] . ") = ";

		if($_POST['poczatkowa'] == "kilometr na godzine"){

			$pomocnicza = kmhNaMs($_POST['zmienna']);

			if($_POST['koncowa'] == "mila na godzine"){

				echo msNaMph($pomocnicza) . "(mph)";
			}
			else if($_POST['koncowa'] == "wezel"){
				
				echo msNaKt($pomocnicza) . "(kt)";
			}
			
			else if($_POST['koncowa'] == "mach"){
				
				echo msNaMa($pomocnicza) . "(ma)";
			}
			
			else if($_POST['koncowa'] == "predkosc swiatla"){
				
				echo msNaC($pomocnicza) . "(c)";
			}
			
			else if($_POST['koncowa'] == "metr na sekunde"){
				
				echo $pomocnicza . "(m/s)";
			}
			else if($_POST['koncowa'] == "kilometr an godzine"){

				echo $_POST['zmienna'] . "(km/h)";
			}
		}
		
		else if($_POST['poczatkowa'] == "mila na godzine"){

			$pomocnicza = mphNaMs($_POST['zmienna']);

			if($_POST['koncowa'] == "kilometr na godzine"){

				echo msNaKmh($pomocnicza) . "(km/h)";
			}
			else if($_POST['koncowa'] == "wezel"){
				
				echo msNaKt($pomocnicza) . "(kt)";
			}
			
			else if($_POST['koncowa'] == "mach"){
				
				echo msNaMa($pomocnicza) . "(ma)";
			}
			
			else if($_POST['koncowa'] == "predkosc swiatla"){
				
				echo msNaC($pomocnicza) . "(c)";
			}
			
			else if($_POST['koncowa'] == "metr na sekunde"){
				
				echo $pomocnicza . "(m/s)";
			}
			else if($_POST['koncowa'] == "mila an godzine"){

				echo $_POST['zmienna'] . "(mph)";
			}
		}
		
		else if($_POST['poczatkowa'] == "wezel"){

			$pomocnicza = ktNaMs($_POST['zmienna']);

			if($_POST['koncowa'] == "kilometr na godzine"){

				echo msNaKmh($pomocnicza) . "(km/h)";
			}
			else if($_POST['koncowa'] == "mila"){
				
				echo msNaMph($pomocnicza) . "(mph)";
			}
			
			else if($_POST['koncowa'] == "mach"){
				
				echo msNaMa($pomocnicza) . "(ma)";
			}
			
			else if($_POST['koncowa'] == "predkosc swiatla"){
				
				echo msNaC($pomocnicza) . "(c)";
			}
			
			else if($_POST['koncowa'] == "metr na sekunde"){
				
				echo $pomocnicza . "(m/s)";
			}
			else if($_POST['koncowa'] == "wezel"){

				echo $_POST['zmienna'] . "(kt)";
			}
		}
		else if($_POST['poczatkowa'] == "mach"){

			$pomocnicza = maNaMs($_POST['zmienna']);

			if($_POST['koncowa'] == "kilometr na godzine"){

				echo msNaKmh($pomocnicza) . "(km/h)";
			}
			else if($_POST['koncowa'] == "mila"){
				
				echo msNaMph($pomocnicza) . "(mph)";
			}
			
			else if($_POST['koncowa'] == "wezel"){
				
				echo msNaKt($pomocnicza) . "(kt)";
			}
			
			else if($_POST['koncowa'] == "predkosc swiatla"){
				
				echo msNaC($pomocnicza) . "(c)";
			}
			
			else if($_POST['koncowa'] == "metr na sekunde"){
				
				echo $pomocnicza . "(m/s)";
			}
			else if($_POST['koncowa'] == "mach"){

				echo $_POST['zmienna'] . "(ma)";
			}
		}
		
		else if($_POST['poczatkowa'] == "predkosc swiatla"){

			$pomocnicza = cNaMs($_POST['zmienna']);

			if($_POST['koncowa'] == "kilometr na godzine"){

				echo msNaKmh($pomocnicza) . "(km/h)";
			}
			else if($_POST['koncowa'] == "mila"){
				
				echo msNaMph($pomocnicza) . "(mph)";
			}
			
			else if($_POST['koncowa'] == "wezel"){
				
				echo msNaKt($pomocnicza) . "(kt)";
			}
			
			else if($_POST['koncowa'] == "mach"){
				
				echo msNaMa($pomocnicza) . "(ma)";
			}
			
			else if($_POST['koncowa'] == "metr na sekunde"){
				
				echo $pomocnicza . "(m/s)";
			}
			else if($_POST['koncowa'] == "predkosc swiatla"){

				echo $_POST['zmienna'] . "(c)";
			}
		}
		
		else if($_POST['poczatkowa'] == "metr na sekunde"){

			if($_POST['koncowa'] == "kilometr na godzine"){

				echo msNaKmh($_POST['zmienna']) . "(km/h)";
			}
			else if($_POST['koncowa'] == "metr na sekunde"){

				echo $_POST['zmienna'] . "(m/s)";
			}
			else if($_POST['koncowa'] == "mila na godzine"){

				echo msNaMph($_POST['zmienna']) . "(mph)";
			}
			else if($_POST['koncowa'] == "wezel"){

				echo msNaKt($_POST['zmienna']) . "(kt)";
			}
			else if($_POST['koncowa'] == "mach"){

				echo msNaMa($_POST['zmienna']) . "(ma)";
			}
			else if($_POST['koncowa'] == "predkosc swiatla"){

				echo msNaC($_POST['zmienna']) . "(C)";
			}
		}
	}	
	