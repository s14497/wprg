<?php

	function atmosfereNaPascale($wartosc){

		return $wartosc * 101325;
	}

	function pascaleNaAtmosfere($wartosc){

		return $wartosc * 0.00000987;
	}
	
	function pascaleNaBary($wartosc){

		return $wartosc / 100000;
	}

	function baryNaPascale($wartosc){

		return $wartosc * 100000;
	}
	
	function psiNaPascale($wartosc){

		return $wartosc * 6894.76;
	}
	
	function pascaleNaPsi($wartosc){

		return (($wartosc * 1.45038) * pow(10, -4));
	}

	include("calc.html");

	if($_POST['zmienna'] != NULL && is_numeric($_POST['zmienna'])){

		echo $_POST['zmienna'] . " (" . $_POST['poczatkowa'] . ") = ";

		if($_POST['poczatkowa'] == "atmosfera"){

			$pomocnicza = atmosfereNaPascale($_POST['zmienna']);

			if($_POST['koncowa'] == "bar"){

				echo pascaleNaBary($pomocnicza) . "(bar)";
			}
			else if($_POST['koncowa'] == "pascal"){

				echo $pomocnicza . "(pascal)";
			}
			else if($_POST['koncowa'] == "psi"){

				echo pascaleNaPsi($pomocnicza) . "(PSI)";
			}
			else if($_POST['koncowa'] == "atmosfera"){

				echo $_POST['zmienna'] . "(atmosfera)";
			}
		}
		else if($_POST['poczatkowa'] == "pascal"){

			if($_POST['koncowa'] == "bar"){

				echo pascaleNaBary($_POST['zmienna']) . "(bar)";
			}
			else if($_POST['koncowa'] == "pascal"){

				echo $_POST['zmienna'] . "(pascal)";
			}
			else if($_POST['koncowa'] == "psi"){

				echo pascaleNaPsi($_POST['zmienna']) . "(PSI)";
			}
			else if($_POST['koncowa'] == "atmosfera"){

				echo pascaleNaAtmosfere($_POST['zmienna']) . "(atmosfer)";
			}
		}
		else if($_POST['poczatkowa'] == "bar"){

			$pomocnicza = baryNaPascale($_POST['zmienna']);

			if($_POST['koncowa'] == "bar"){

				echo $_POST['zmienna'] . "(bar)";
			}
			else if($_POST['koncowa'] == "pascal"){

				echo $pomocnicza . "(pascal)";
			}
			else if($_POST['koncowa'] == "psi"){

				echo pascaleNaPsi($pomocnicza) . "(PSI)";
			}
			else if($_POST['koncowa'] == "atmosfera"){

				echo pascaleNaAtmosfere($pomocnicza) . "(atmosfera)";
			}
		}
		else if($_POST['poczatkowa'] == "psi"){

			$pomocnicza = psiNaPascale($_POST['zmienna']);

			if($_POST['koncowa'] == "bar"){

				echo pascaleNaBary($pomocnicza) . "(bar)";
			}
			else if($_POST['koncowa'] == "pascal"){

				echo $pomocnicza . "(pascal)";
			}
			else if($_POST['koncowa'] == "psi"){

				echo $_POST['zmienna'] . "(PSI)";
			}
			else if($_POST['koncowa'] == "atmosfera"){

				echo pascaleNaAtmosfere($pomocnicza) . "(atmosfera)";
			}
		}
	}
?>