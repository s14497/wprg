<?php

	function metrNaKilometr ($wartosc){
		return $wartosc * 0.001;
	}

	function kilometrNaMetr ($wartosc){
		return $wartosc * 1000;
	}
	
	function metrNaCal($wartosc){
		return $wartosc * 39.37;
	}
	
	function calNaMetr($wartosc){
		return $wartosc * 0.0254;
	}
	
	function metrNaStopa($wartosc){
		return $wartosc * 3.281;
	}

	function stopaNaMetr($wartosc){
		return $wartosc * 0.3048;
	}
	
	function metrNaJard ($wartosc){
		return $wartosc * 1.094;
	}

	function jardNaMetr ($wartosc){
		return $wartosc * 0.9144;
	}
	
	function metrNaMila ($wartosc){
		return $wartosc * 0.000621;
	}

	function milaNaMetr ($wartosc){
		return $wartosc * 1609.34;
	}
	
	include("calc.html");

	if($_POST['zmienna'] != NULL && is_numeric($_POST['zmienna'])){

		echo $_POST['zmienna'] . " (" . $_POST['poczatkowa'] . ") = ";

		if($_POST['poczatkowa'] == "kilometr"){

			$pomocnicza = kilometrNaMetr($_POST['zmienna']);

			if($_POST['koncowa'] == "cal"){

				echo metrNaCal($pomocnicza) . "(cal)";
			}
			else if($_POST['koncowa'] == "stopa"){
				
				echo metrNaStopa($pomocnicza) . "(stopa)";
			}
			
			else if($_POST['koncowa'] == "jard"){
				
				echo metrNaJard($pomocnicza) . "(jard)";
			}
			
			else if($_POST['koncowa'] == "mila"){
				
				echo metrNaMila($pomocnicza) . "(mila)";
			}
			
			else if($_POST['koncowa'] == "metr"){
				
				echo $pomocnicza . "(metr)";
			}
			else if($_POST['koncowa'] == "kilometr"){

				echo $_POST['zmienna'] . "(kilometr)";
			}
		}
		
		else if($_POST['poczatkowa'] == "cal"){

			$pomocnicza = calNaMetr($_POST['zmienna']);

			if($_POST['koncowa'] == "stopa"){

				echo metrNaStopa($pomocnicza) . "(stopa)";
			}
			else if($_POST['koncowa'] == "jard"){
				
				echo metrNaJard($pomocnicza) . "(jard)";
			}
			
			else if($_POST['koncowa'] == "mila"){
				
				echo metrNaMila($pomocnicza) . "(mila)";
			}
			
			else if($_POST['koncowa'] == "kilometr"){
				
				echo metrNaKilometr($pomocnicza) . "(kilometr)";
			}
			
			else if($_POST['koncowa'] == "metr"){
				
				echo $pomocnicza . "(metr)";
			}
			else if($_POST['koncowa'] == "cal"){

				echo $_POST['zmienna'] . "(cal)";
			}
		}
		
		else if($_POST['poczatkowa'] == "stopa"){

			$pomocnicza = stopaNaMetr($_POST['zmienna']);

			if($_POST['koncowa'] == "cal"){

				echo metrNaCal($pomocnicza) . "(cal)";
			}
			else if($_POST['koncowa'] == "jard"){
				
				echo metrNaJard($pomocnicza) . "(jard)";
			}
			
			else if($_POST['koncowa'] == "mila"){
				
				echo metrNaMila($pomocnicza) . "(mila)";
			}
			
			else if($_POST['koncowa'] == "kilometr"){
				
				echo metrNaKilometr($pomocnicza) . "(kilometr)";
			}
			
			else if($_POST['koncowa'] == "metr"){
				
				echo $pomocnicza . "(metr)";
			}
			else if($_POST['koncowa'] == "stopa"){

				echo $_POST['zmienna'] . "(stopa)";
			}
		}
		
		else if($_POST['poczatkowa'] == "jard"){

			$pomocnicza = jardNaMetr($_POST['zmienna']);

			if($_POST['koncowa'] == "cal"){

				echo metrNaCal($pomocnicza) . "(cal)";
			}
			else if($_POST['koncowa'] == "stopa"){
				
				echo metrNaStopa($pomocnicza) . "(stopa)";
			}
			
			else if($_POST['koncowa'] == "mila"){
				
				echo metrNaMila($pomocnicza) . "(mila)";
			}
			
			else if($_POST['koncowa'] == "kilometr"){
				
				echo metrNaKilometr($pomocnicza) . "(kilometr)";
			}
			
			else if($_POST['koncowa'] == "metr"){
				
				echo $pomocnicza . "(metr)";
			}
			else if($_POST['koncowa'] == "jard"){

				echo $_POST['zmienna'] . "(jard)";
			}
		}
		
		else if($_POST['poczatkowa'] == "mila"){

			$pomocnicza = milaNaMetr($_POST['zmienna']);

			if($_POST['koncowa'] == "cal"){

				echo metrNaCal($pomocnicza) . "(cal)";
			}
			else if($_POST['koncowa'] == "stopa"){
				
				echo metrNaStopa($pomocnicza) . "(stopa)";
			}
			
			else if($_POST['koncowa'] == "jard"){
				
				echo metrNaJard($pomocnicza) . "(jard)";
			}
			
			else if($_POST['koncowa'] == "kilometr"){
				
				echo metrNaKilometr($pomocnicza) . "(kilometr)";
			}
			
			else if($_POST['koncowa'] == "metr"){
				
				echo $pomocnicza . "(metr)";
			}
			else if($_POST['koncowa'] == "mila"){

				echo $_POST['zmienna'] . "(mila)";
			}
		}
		
		else if($_POST['poczatkowa'] == "metr"){

			if($_POST['koncowa'] == "cal"){

				echo metrNaCal($_POST['zmienna']) . "(cal)";
			}
			else if($_POST['koncowa'] == "metr"){

				echo $_POST['zmienna'] . "(metr)";
			}
			else if($_POST['koncowa'] == "kilometr"){

				echo metrNaKilometr($_POST['zmienna']) . "(kilometr)";
			}
			else if($_POST['koncowa'] == "stopa"){

				echo metrNaStopa($_POST['zmienna']) . "(stopa)";
			}
			else if($_POST['koncowa'] == "jard"){

				echo metrNaJard($_POST['zmienna']) . "(jard)";
			}
			else if($_POST['koncowa'] == "mila"){

				echo metrNaMila($_POST['zmienna']) . "(mila)";
			}
		}
	}	
	