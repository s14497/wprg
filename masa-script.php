<?php

	function gramNaFunt ($wartosc){

		return $wartosc * 0.0022;
	}

	function funtNaGram ($wartosc){

		return $wartosc * 453.59;
	}
	
	function gramNaUncja($wartosc){

		return $wartosc * 0.03527;
	}
	
	function uncjaNaGram($wartosc){

		return $wartosc * 28.35;
	}
	
	function gramNaKarat($wartosc){

		return $wartosc * 5;
	}

	function karatNaGram($wartosc){

		return $wartosc * 0.2;
	}
	
	
	include("calc.html");

	if($_POST['zmienna'] != NULL && is_numeric($_POST['zmienna'])){

		echo $_POST['zmienna'] . " (" . $_POST['poczatkowa'] . ") = ";

		if($_POST['poczatkowa'] == "funt"){

			$pomocnicza = funtNaGram($_POST['zmienna']);

			if($_POST['koncowa'] == "uncja"){

				echo gramNaUncja($pomocnicza) . "(uncja)";
			}
			else if($_POST['koncowa'] == "karat"){
				
				echo gramNaKarat($pomocnicza) . "(karat)";
			}
			else if($_POST['koncowa'] == "gram"){
				
				echo $pomocnicza . "(gram)";
			}
			else if($_POST['koncowa'] == "funt"){

				echo $_POST['zmienna'] . "(funt)";
			}
		}
		
		else if($_POST['poczatkowa'] == "gram"){

			if($_POST['koncowa'] == "funt"){

				echo gramNaFunt($_POST['zmienna']) . "(funt)";
			}
			else if($_POST['koncowa'] == "gram"){

				echo $_POST['zmienna'] . "(gram)";
			}
			else if($_POST['koncowa'] == "uncja"){

				echo gramNaUncja($_POST['zmienna']) . "(uncja)";
			}
			else if($_POST['koncowa'] == "karat"){

				echo gramNaKarat($_POST['zmienna']) . "(karat)";
			}
		}
		
		else if($_POST['poczatkowa'] == "uncja"){

			$pomocnicza = uncjaNaGram($_POST['zmienna']);

			if($_POST['koncowa'] == "funt"){

				echo gramNaFunt($pomocnicza) . "(funt)";
			}
			else if($_POST['koncowa'] == "karat"){
				
				echo gramNaKarat($pomocnicza) . "(karat)";
			}
			else if($_POST['koncowa'] == "gram"){
				
				echo $pomocnicza . "(gram)";
			}
			else if($_POST['koncowa'] == "uncja"){

				echo $_POST['zmienna'] . "(uncja)";
			}
		}
		
		else if($_POST['poczatkowa'] == "karat"){

			$pomocnicza = karatNaGram($_POST['zmienna']);

			if($_POST['koncowa'] == "funt"){

				echo gramNaFunt($pomocnicza) . "(funt)";
			}
			else if($_POST['koncowa'] == "uncja"){
				
				echo gramNaUncja($pomocnicza) . "(uncja)";
			}
			else if($_POST['koncowa'] == "gram"){
				
				echo $pomocnicza . "(gram)";
			}
			else if($_POST['koncowa'] == "karat"){

				echo $_POST['zmienna'] . "(karat)";
			}
		}
	}
?>